﻿using UnityEngine;
using System.Collections;

public class MainMenuView : View
{
    public void OnPlay()
    {
        ViewManager.Instance.SwitchToView<GameView>();
    }

    public void OnTopScores()
    {
        ViewManager.Instance.SwitchToView<SeeRecordView>();
    }

    public void OnExit()
    {
        Application.Quit();
    }
}
