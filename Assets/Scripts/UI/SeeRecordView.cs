﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SeeRecordView : View
{
    public int MaxPlayersShown = 10;

    [SerializeField] private GameObject _placeHolder;

    public void OnMainMenu()
    {
        ViewManager.Instance.SwitchToView<MainMenuView>();
    }

    protected override void OnShowStart()
    {
        base.OnShowStart();
        string result = "";
        if (GameSettings.Instance.LoadedData != null)
        {
            MaxPlayersShown = Mathf.Min(GameSettings.Instance.LoadedData.Count, MaxPlayersShown);
            for (int i = 0; i < MaxPlayersShown; i++)
            {
                string name = GameSettings.Instance.LoadedData[i].Name;
                int score = GameSettings.Instance.LoadedData[i].Score;
                result += name + " ........ " + score + "\n";
            }
        }
        _placeHolder.GetComponent<Text>().text = result;
    }
}
