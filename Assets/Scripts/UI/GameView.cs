﻿using UnityEngine;
using System.Collections;

public class GameView : View
{
    public void OnPause()
    {
        ViewManager.Instance.SwitchToView<PauseView>();
    }

    protected override void OnShowComplete()
    {
        base.OnShowComplete();
        GameManager.Instance.StartGame();
    }
}
