﻿using UnityEngine;
using System.Collections;

public class PauseView : View
{
    public void OnResume()
    {
        GameManager.Instance.ResumeGame();
        ViewManager.Instance.SwitchToView<GameView>();
    }

    public void OnMainMenu()
    {
        GameManager.Instance.GameOver();
        GameManager.Instance.ResumeGame();
        ViewManager.Instance.SwitchToView<MainMenuView>();
    }

    public void OnReset()
    {
        GameManager.Instance.ResetGame();
        ViewManager.Instance.SwitchToView<GameView>();
    }

    protected override void OnShowComplete()
    {
        base.OnShowComplete();
        GameManager.Instance.PauseGame();
    }
}
