﻿// Created by Fouad Valadbeigi <pixelomaniak@gmail.com> on 26/04/2016.

using UnityEngine;
using System.Collections;

/// <summary>
/// A manager for the UI(view) elements.
/// </summary>
public class ViewManager : Singleton<ViewManager>
{
    private View[] _views;

    public void SwitchToView<T>() where T : View
    {
        SwitchToView(GetView<T>());
    }

    /// <summary>
    /// Hides the current view and shows the view.
    /// </summary>
    /// <param name="view">View to show</param>
    public void SwitchToView(View view)
    {
        foreach(var v in _views)
        {
            if (v.IsVisible)
            {
                v.Hide();
            }
        }
        view.Show();
    }

    private View GetView<T>() where T : View
    {
        foreach(var v in _views)
        {
            if(v is T)
            {
                return v as T;
            }
        }
        Debug.Log("No view: " + typeof(T));
        return null;
    }
 
    private void Start()
    {
        _views = GetComponentsInChildren<View>(true);
    }
}
