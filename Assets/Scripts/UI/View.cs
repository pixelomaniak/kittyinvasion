﻿// Created by Fouad Valadbeigi <pixelomaniak@gmail.com> on 26/04/2016.

using UnityEngine;
using System.Collections;
using DG.Tweening;

/// <summary>
/// A class for managing the UI elements.
/// </summary>
[RequireComponent(typeof(CanvasGroup))]
public class View : MonoBehaviour
{
    public bool IsActiveOnStart = false;
    // How long it takes for the UI elemnt to fade in/out.
    public float FadingDuration = 1.0f;
    [HideInInspector] public bool IsVisible = false;
    
    [SerializeField] private CanvasGroup _cg;

    /// <summary>
    /// Fading in shows the UI element.
    /// </summary>
    public void Show()
    {
        gameObject.SetActive(true);
        IsVisible = true;
        DOTween.To(() => _cg.alpha, value => _cg.alpha = value, 1.0f, FadingDuration).OnStart(OnShowStart).OnComplete(OnShowComplete);
    }

    /// <summary>
    /// Fading out hides the UI element.
    /// </summary>
    public void Hide()
    {
        IsVisible = false;
        DOTween.To(() => _cg.alpha, value => _cg.alpha = value, 0.0f, FadingDuration).OnStart(OnHideStart).OnComplete(OnHideComplete);
    }

    protected virtual void Start()
    {
        _cg = GetComponent<CanvasGroup>();
        if (!IsActiveOnStart)
        {
            _cg.alpha = 0.0f;
            SetInteraction(false);
            gameObject.SetActive(false);
        }
        else
        {
            SetInteraction(true);
            IsVisible = true;
        }
    }

    protected virtual void OnShowStart() { }

    protected virtual void OnShowComplete()
    {
        SetInteraction(true);
    }

    protected virtual void OnHideStart()
    {
        SetInteraction(false);
    }

    protected virtual void OnHideComplete()
    {
        gameObject.SetActive(false);
    }

    private void SetInteraction(bool state)
    {
        //_cg.interactable = state;
        _cg.blocksRaycasts = state;
    }
}
