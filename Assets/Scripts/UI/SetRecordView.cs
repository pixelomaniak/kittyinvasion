﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetRecordView : View
{
    public GameObject PlayerScore;
    public GameObject PlayerName;

    // Ultra dumb version for now!
    public void OnSubmit()
    {
        // TODO: Add format checking
        string playerName = PlayerName.GetComponent<InputField>().text;
        playerName = playerName.Trim().ToUpper();
        if(playerName.Length > 0)
        {
            GameSettings.Instance.PlayerName = playerName;
            GameSettings.Instance.SaveData();
            ViewManager.Instance.SwitchToView<MainMenuView>();
        }
    }

    public void OnMainMenu()
    {
        ViewManager.Instance.SwitchToView<MainMenuView>();
    }

    protected override void OnShowComplete()
    {
        base.OnShowComplete();
        PlayerScore.GetComponent<Text>().text = "Score: " + GameSettings.Instance.Score;
    }
}
