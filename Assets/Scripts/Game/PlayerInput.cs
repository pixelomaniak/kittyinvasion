﻿// Created by Fouad Valadbeigi <pixelomaniak@gmail.com> on 26/04/2016.

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

/// <summary>
/// A class for taking care of the input from the user either from keyboard or screen taps.
/// </summary>
public class PlayerInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public enum ScreenSide
    {
        Left,
        Right
    }

    public delegate void OnTouchEvent(ScreenSide screenSide);
    public event OnTouchEvent OnTouchDown;
    public event OnTouchEvent OnTouchUp;

#if UNITY_EDITOR
    private void Update ()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (OnTouchDown != null)
            {
                OnTouchDown(ScreenSide.Left);
            }
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (OnTouchDown != null)
            {
                OnTouchDown(ScreenSide.Right);
            }
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            if (OnTouchUp != null)
            {
                OnTouchUp(ScreenSide.Left);
            }
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            if (OnTouchUp != null)
            {
                OnTouchUp(ScreenSide.Right);
            }
        }
    }

#endif
    public void OnPointerDown(PointerEventData eventData)
    {
        ScreenSide side = eventData.position.x < Screen.width / 2 ? ScreenSide.Left : ScreenSide.Right;
        if (OnTouchDown != null)
        {
            OnTouchDown(side);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("OnPointerUp!");
        ScreenSide side = eventData.position.x < Screen.width / 2 ? ScreenSide.Left : ScreenSide.Right;
        if (OnTouchUp != null)
        {
            OnTouchUp(side);
        }
    }
}
