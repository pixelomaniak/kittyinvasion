﻿// Created by Fouad Valadbeigi <pixelomaniak@gmail.com> on 26/04/2016.

using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public System.Action OnGameOver;
    public System.Action OnGameStarted;

    private bool _isPlaying = false;

    public void GameOver(bool showSetRecordView = true)
    {
        _isPlaying = false;
        if(OnGameOver != null)
        {
            OnGameOver();
            // TODO: Save scores
        }
        PoolManager.Instance.CleanUp(false);
        PlayerSpawner.Instance.CleanUp();
        EnemySpawner.Instance.CleanUp();
        if (showSetRecordView)
        {
            ViewManager.Instance.SwitchToView<SetRecordView>();
        }
        SoundManager.Instance.BackgroundMusic.Stop();
    }

    public void StartGame()
    {
        ResumeGame();
        if (_isPlaying)
        {
            return;
        }
        PlayerSpawner.Instance.Spawn();
        EnemySpawner.Instance.Spawn();
        _isPlaying = true;
        GameSettings.Instance.Reset();
        if (OnGameStarted != null)
        {
            OnGameStarted();
        }
        SoundManager.Instance.BackgroundMusic.Play();
    }
    
    public void PauseGame()
    {
        Time.timeScale = 0.0f;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1.0f;
    }

    public void ResetGame()
    {
        GameOver(false);
        StartGame();
    }
}
