﻿// Created by Fouad Valadbeigi <pixelomaniak@gmail.com> on 26/04/2016.

using UnityEngine;

/// <summary>
/// A singleton for spawning a player object.
/// </summary>
public class PlayerSpawner : Singleton<PlayerSpawner>
{
    public GameObject Player;

    private GameObject _currentClone;

    /// <summary>
    /// Spawns a player object.
    /// </summary>
    public void Spawn()
    {
        SpawnPlayer();
    }

    /// <summary>
    /// Destroys the current instance of the player object.
    /// </summary>
    public void CleanUp()
    {
        if (_currentClone != null)
        {
            Destroy(_currentClone);
        }
    }

    private void SpawnPlayer()
    {
        _currentClone = Instantiate(Player);
        _currentClone.transform.parent = gameObject.transform;
        float y = transform.position.y;
        float z = transform.position.z;
        _currentClone.transform.position = new Vector3(0.0f, y, z);
    }

}
