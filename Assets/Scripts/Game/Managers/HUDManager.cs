﻿using UnityEngine;
using UnityEngine.UI;

public class HUDManager : Singleton<HUDManager>
{
    [SerializeField] private GameObject LivesHUD;
    [SerializeField] private GameObject ScoreHUD;

    public void SetLives(int numLives)
    {
        LivesHUD.GetComponent<Text>().text = "Lives: " + numLives;
    }

    public void SetScore(int score)
    {
        ScoreHUD.GetComponent<Text>().text = "Score: " + score;
    }
}
