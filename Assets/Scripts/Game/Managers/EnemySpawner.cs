﻿// Created by Fouad Valadbeigi <pixelomaniak@gmail.com> on 26/04/2016.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A singleton for spawning a enemy objects.
/// </summary>
public class EnemySpawner : Singleton<EnemySpawner>
{
    public GameObject Kitty;
    public float EnemySpawnRate = 5.0f;

    private bool _keepSpawning = true;
    private List<GameObject> _currentClones = new List<GameObject>();

    /// <summary>
    /// Spawns enemy objects frequently based on the EnemySpawnRate.
    /// </summary>
    public void Spawn()
    {
        _keepSpawning = true;
        StopAllCoroutines();
        StartCoroutine(SpawnEnemies());
    }

    /// <summary>
    /// Destroys the currnet enemy objects in the scene.
    /// </summary>
    public void CleanUp()
    {
        foreach (var go in _currentClones)
        {
            if (go != null)
            {
                Destroy(go);
            }
        }

        _keepSpawning = false;
        StopCoroutine(SpawnEnemies());
    }

    IEnumerator SpawnEnemies()
    {
        while (_keepSpawning)
        {
            GameObject obj = Instantiate(Kitty);
            obj.transform.parent = gameObject.transform;
            float y = transform.position.y;
            float z = transform.position.z;
            obj.transform.position = new Vector3(Random.Range(GameSettings.Instance.BoardBoundary.Min, GameSettings.Instance.BoardBoundary.Max), y, z);
            Mover mover = obj.GetComponent<Mover>();
            mover.Direction = transform.forward;
            _currentClones.Add(obj);
            yield return new WaitForSeconds(EnemySpawnRate);
        }
        yield return null;
    }
}
