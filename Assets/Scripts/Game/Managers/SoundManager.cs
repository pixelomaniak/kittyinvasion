﻿using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

/// <summary>
/// A manager for the sound effects as well 
/// as the main background music of the game.
/// </summary>
public class SoundManager : Singleton<SoundManager>
{
    public AudioSource BackgroundMusic;
    public float MinPitch = 0.95f;
    public float MaxPitch = 1.05f;
    public float MinVolume = 0f;
    public float MaxVolume = 1f;

    private List<AudioSource> _audioSourcePool;
    private int _poolSize = 3;

    /// <summary>
    /// Plays back an audio clip with or without delay.
    /// </summary>
    /// <param name="clip">Audio clip to play</param>
    /// <param name="delay">Plays the clip after the given delay in seconds</param>
    public void PlaySound(AudioClip clip, float delay = 0f)
    {
        AudioSource source = GetAvailableAudioSource();
        PlaySound(source, clip, delay);
    }

    /// <summary>
    /// Given a list of audio clips picks one randomly and plays it.
    /// </summary>
    /// <param name="clips">List of audio clips</param>
    /// <param name="randomiseVolume">If the volume level should be random</param>
    /// <param name="randomisePitch">If the pitch level should be random</param>
    public void PlaySoundRandomise(AudioClip[] clips, bool randomiseVolume = false, bool randomisePitch = true)
    {
        AudioSource source = GetAvailableAudioSource();
        float volume = randomiseVolume ? Random.Range(MinVolume, MaxVolume) : 1f;
        float pitch = randomisePitch ? Random.Range(MinPitch, MaxPitch) : 1f;
        source.volume = volume;
        source.pitch = pitch;

        int randID = Random.Range(0, clips.Length);
        AudioClip clip = clips[randID];

        PlaySound(source, clip, 0);
    }

    private void Start()
    {
        _audioSourcePool = new List<AudioSource>();
        for(int i = 0; i < _poolSize; i++)
        {
            AudioSource source = gameObject.AddComponent<AudioSource>();
            source.playOnAwake = false;
            _audioSourcePool.Add(source);
        }
    }

    private void PlaySound(AudioSource source, AudioClip clip, float delay = 0f)
    {
        source.clip = clip;
        if (delay > 0f)
        {
            source.PlayDelayed(delay);
        }
        else
        {
            source.Play();
        }
    }

    // Returns an audio source that is not currently playing from the pool of audio sources.
    private AudioSource GetAvailableAudioSource()
    {
        foreach(var audioSource in _audioSourcePool)
        {
            if (!audioSource.isPlaying)
            {
                return audioSource;
            }
            else
            {
                AudioSource newAudioSource = gameObject.AddComponent<AudioSource>();
                newAudioSource.playOnAwake = false;
                _audioSourcePool.Add(newAudioSource);
                return newAudioSource;
            }
        }
        return null;
    }
}
