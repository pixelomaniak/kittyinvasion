﻿// Created by Fouad Valadbeigi <pixelomaniak@gmail.com> on 26/04/2016.

using System;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// A singleton that keeps track of the score.
/// and is responsible for loading and saving player info when needed.
/// </summary>
public class GameSettings : Singleton<GameSettings>
{
    [Serializable]
    public struct PlayerData
    {
        public string Name;
        public int Score;
    }
    [Serializable]
    public struct Boundary
    {
        public float Min;
        public float Max;
    }

    public Boundary BoardBoundary;
    private int _score;
    public int Score
    {
        set
        {
            _score = value;
        }
        get
        {
            return _score;
        }
    }
    [HideInInspector] public string PlayerName;
    [HideInInspector] public List<PlayerData> LoadedData;

    [SerializeField] private int _maxCapacity = 10;

    public void Reset()
    {
        Score = 0;
        HUDManager.Instance.SetScore(0);
    }

    /// <summary>
    /// Creates a new PlayerData struct and will try to save it.
    /// </summary>
    public void SaveData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/KittyInvasionTopScores.kittyinvasion", FileMode.OpenOrCreate);
        List<PlayerData> data;
        PlayerData playerData = new PlayerData();
        playerData.Name = PlayerName;
        playerData.Score = Score;
        if (LoadedData != null)
        {
            bool alreadySaved = false;
            for(int i = 0; i < LoadedData.Count; i++)
            {
                string name = LoadedData[i].Name;
                int score = LoadedData[i].Score;
                if(name == playerData.Name)
                {
                    alreadySaved = true;
                    LoadedData[i] = score < playerData.Score ? playerData : LoadedData[i];
                    break;
                }
            }
            if (!alreadySaved)
            {
                LoadedData.Add(playerData);
            }
            LoadedData.Sort((a, b) => b.Score - a.Score);
            data = new List<PlayerData>(LoadedData);
        }
        else
        {
            data = new List<PlayerData>();
            data.Add(playerData);
        }
        bf.Serialize(file, data);
        file.Close();
    }

    /// <summary>
    /// Tris to load previously saved data(if any) into LoadedData list.
    /// </summary>
    public void LoadData()
    {
        Debug.Log(Application.persistentDataPath);
        if (File.Exists(Application.persistentDataPath + "/KittyInvasionTopScores.kittyinvasion"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/KittyInvasionTopScores.kittyinvasion", FileMode.Open);
            LoadedData = new List<PlayerData>();
            try
            {
                LoadedData = (List<PlayerData>)bf.Deserialize(file);
            }
            catch(Exception e)
            {
                Debug.Log(e);
                LoadedData = null;
                file.Close();
                return;
            }
            //foreach(var i in LoadedData)
            //{
            //    Debug.Log(i.Name + i.Score);
            //}
            file.Close();
        }
    }

    private void Awake()
    {
        LoadData();
    }
}
