﻿using UnityEngine;
using System.Collections;
using System;

public class Player : LiveAgent
{
    public float FireRate = 1;

    protected override void Fire()
    {
        base.Fire();
        GameObject bullet = PoolManager.Instance.Pools["Bullet"].GetPooledObject();
        if (bullet == null)
        {
            return;
        }
        Mover mover = bullet.GetComponent<Mover>();
        mover.Direction = transform.forward;
        bullet.transform.position = transform.position;
        bullet.transform.rotation = transform.rotation;
        bullet.SetActive(true);
    }

    protected override void RemoveFromScene()
    {
        base.RemoveFromScene();
        GameManager.Instance.GameOver();
    }

    protected override void OnHit()
    {
        base.OnHit();
        HUDManager.Instance.SetLives(NumLives);
    }

    private void Start()
    {
        HUDManager.Instance.SetLives(NumLives);
        InvokeRepeating("Fire", 0, 1.0f / FireRate);
    }
}
