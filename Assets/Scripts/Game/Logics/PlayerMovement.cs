﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Attached to the "player" this class reads the input from
/// an instance of PlayerInput class and is responsible for moving
/// the player around.
/// </summary>
public class PlayerMovement : MonoBehaviour
{
    public float MovingSpeed = 1;

    private new Rigidbody rigidbody;
    private PlayerInput _input;

    public void OnTouchDownInput(PlayerInput.ScreenSide screenSide)
    {
        Vector3 movingDirection = new Vector3(screenSide == PlayerInput.ScreenSide.Left ? -1 : 1, 0, 0);
        rigidbody.velocity = movingDirection * MovingSpeed;
    }

    public void OnTouchUpInput(PlayerInput.ScreenSide screenSide)
    {
        rigidbody.velocity = Vector3.zero;
    }

    private void Start()
    {
        _input = FindObjectOfType<PlayerInput>();
        if (_input != null)
        {
            _input.OnTouchDown += OnTouchDownInput;
            _input.OnTouchUp += OnTouchUpInput;
        }
        rigidbody = GetComponent<Rigidbody>();
    }

    private void OnDisable()
    {
        if (_input != null)
        {
            _input.OnTouchDown -= OnTouchDownInput;
            _input.OnTouchUp -= OnTouchUpInput;
        }
    }

    private void FixedUpdate()
    {
        rigidbody.position = new Vector3
        (
            Mathf.Clamp(rigidbody.position.x, GameSettings.Instance.BoardBoundary.Min, GameSettings.Instance.BoardBoundary.Max),
            rigidbody.position.y,
            rigidbody.position.z
        );
    }
}
