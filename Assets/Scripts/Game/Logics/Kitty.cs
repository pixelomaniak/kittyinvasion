﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class Kitty : LiveAgent
{
    public float FireRate = 1;

    protected override void Fire()
    {
        base.Fire();
        string bulletType = Random.value < (2.0 / 3.0) ? "Poop" : "Hairball";
        GameObject bullet = PoolManager.Instance.Pools[bulletType].GetPooledObject();
        if(bullet == null)
        {
            return;
        }
        Mover mover = bullet.GetComponent<Mover>();
        mover.Direction = transform.forward;
        bullet.transform.position = transform.position;
        bullet.transform.rotation = transform.rotation;
        bullet.SetActive(true);
    }

    private void Start()
    {
        InvokeRepeating("Fire", 0, 1.0f / FireRate);
    }
}
