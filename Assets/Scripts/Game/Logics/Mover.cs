﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour
{
    public float Speed = 1;
    public Vector3 Direction = Vector3.forward;

    private Rigidbody _rb;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _rb.velocity = Direction * Speed;
    }
}
