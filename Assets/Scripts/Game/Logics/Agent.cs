﻿// Created by Fouad Valadbeigi <pixelomaniak@gmail.com> on 26/04/2016.

using UnityEngine;
using System.Collections;

/// <summary>
/// Every "game object" in the game is an Agent instance.
/// </summary>
public class Agent : MonoBehaviour
{
    public bool OnlyInactivateOnDestroy = false;
    public GameObject Explosion;
    public AudioClip[] ExplosionSounds;
    public int Value = 0;

    /// <summary>
    /// Removes the agent from th scene either by destorying it or 
    /// by making it inactive.
    /// </summary>
    protected virtual void RemoveFromScene()
    {
        if (OnlyInactivateOnDestroy)
        {
            gameObject.SetActive(false);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Displays explosion and sound when an agent should be destoryed
    /// and calls the RemoveFromScene.
    /// </summary>
    protected virtual void Die()
    {
        GameSettings.Instance.Score += Value;
        HUDManager.Instance.SetScore(GameSettings.Instance.Score);
        if(Explosion != null)
        {
            Instantiate(Explosion, transform.position, transform.rotation);
        }
        if(ExplosionSounds.Length > 0)
        {
            SoundManager.Instance.PlaySoundRandomise(ExplosionSounds);
        }
        RemoveFromScene();
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Boundary") || other.gameObject.layer == gameObject.layer)
        {
            return;
        }
        Die();
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Boundary"))
        {
            RemoveFromScene();
        }
    }
}
