﻿using UnityEngine;
using System.Collections;

/// <summary>
/// All "live" agents in the game i.e Player, enemy
/// and NOT bullet, enemy shots, etc are instances of this class
/// or it's derived classes.
/// </summary>
public class LiveAgent : Agent
{
    public int NumLives = 3;
    public AudioClip[] HitSounds;
    protected bool IsDead = false;

    /// <summary>
    /// Called when the live agent gets hit.
    /// It decrements NumLives and calls Die if necessary.
    /// </summary>
    protected virtual void OnHit()
    {
        NumLives--;
        if(HitSounds.Length > 0)
        {
            SoundManager.Instance.PlaySoundRandomise(HitSounds);
        }
        if (NumLives <= 0)
        {
            Die();
        }
    }

    /// <summary>
    /// Called when the live agent should fire bullets, shots, etc.
    /// </summary>
    protected virtual void Fire() { }

    protected new void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boundary"))
        {
            return;
        }
        OnHit();
    }
}
