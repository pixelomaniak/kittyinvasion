﻿// Created by Fouad Valadbeigi <pixelomaniak@gmail.com> on 26/04/2016.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A singleton to create/manage Pools in the game.
/// </summary>
public class PoolManager : Singleton<PoolManager>
{
    [Header("Pool properties")]
    public int PoolSize = 20;
    public bool Expandable = true;
    [Header("Pooled objects")]
    public GameObject[] PooledObjects;

    [HideInInspector] public Dictionary<string, Pool> Pools;

    /// <summary>
    /// Cleans up all the current pools.
    /// </summary>
    /// <param name="destroy">Whether the pools should be completely destroyed or just be inactived</param>
    public void CleanUp(bool destroy)
    {
        foreach(var pool in Pools.Values)
        {
            pool.CleanUp(destroy);
        }
        if (destroy)
        {
            Pools.Clear();
        }
    }

    private void Start()
    {
        InitPool();
    }

    private void InitPool()
    {
        Pools = new Dictionary<string, Pool>();
        for (int i = 0; i < PooledObjects.Length; i++)
        {
            GameObject go = PooledObjects[i];
            if (go != null)
            {
                CreateObjectPool(go);
            }
        }
    }

    private void CreateObjectPool(GameObject pooledObject)
    {
        if (!Pools.ContainsKey(pooledObject.name))
        {
            Pool pool = new Pool(pooledObject, PoolSize, Expandable);
            Pools.Add(pooledObject.name, pool);
        }
    }
}
