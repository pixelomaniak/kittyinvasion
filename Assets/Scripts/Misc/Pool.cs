﻿// Created by Fouad Valadbeigi <pixelomaniak@gmail.com> on 26/04/2016.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A class to create object pools.
/// </summary>
public class Pool
{
    private List<GameObject> _pooledObjects;
    private GameObject _pooledObject;
    private int _poolSize = 10;
    private bool _expandable = true;

    /// <summary>
    /// Create a pool of game objects.
    /// </summary>
    /// <param name="pooledObject">Game object to used to create the pool</param>
    /// <param name="poolSize">Size of the pool</param>
    /// <param name="expanable">Whether the size of the pool increases when necessary or it remains fixed</param>
    public Pool(GameObject pooledObject, int poolSize, bool expanable)
    {
        this._pooledObjects = new List<GameObject>();
        this._pooledObject = pooledObject;
        this._poolSize = poolSize;
        this._expandable = expanable;

        for (int i = 0; i < _poolSize; i++)
        {
            GameObject obj = (GameObject)GameObject.Instantiate(this._pooledObject);
            obj.SetActive(false);
            _pooledObjects.Add(obj);
        }
    }

    /// <summary>
    /// Retunrs an "available"(i.e inactive in heirarchy) game object from the pool.
    /// </summary>
    /// <returns></returns>
    public GameObject GetPooledObject()
    {
        for(int i = 0; i < _pooledObjects.Count; i++)
        {
            GameObject obj = _pooledObjects[i];
            if (!obj.activeInHierarchy)
            {
                return obj;
            }
        }
        if (_expandable)
        {
            GameObject obj = Object.Instantiate(_pooledObject);
            obj.SetActive(false);
            _pooledObjects.Add(obj);
            return obj;
        }
        return null;
    }

    /// <summary>
    /// Cleans up the pool.
    /// </summary>
    /// <param name="destroy">Whether the game objects in the pool should be destroyed or just be inactivated</param>
    public void CleanUp(bool destroy)
    {
        if (destroy)
        {
            foreach (var go in _pooledObjects)
            {
                GameObject.Destroy(go);
            }
            _pooledObjects.Clear();
        }
        else
        {
            foreach (var go in _pooledObjects)
            {
                go.SetActive(false);
            }
        }
    }
}
